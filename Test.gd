#
# Random Number Generation (RNG)
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2019-2021 Leandro Motta Barros
#

extends SceneTree

var RNG
var Assert
var Stats

const N := 10000 # standard number of iterations in each test

func _init():
	RNG = preload("res://RNG.gd")
	Assert = preload("res://Assert.gd")
	Stats = preload("res://Stats.gd")

	TestSameSeed()
	TestDifferentSeed()

	TestUniformDefault()
	TestUniform(0.0, 1.0)
	TestUniform(-1.0, 1.0)
	TestUniform(-2.0, -1.0)
	TestUniform(-1.33, 0.0)
	TestUniform(-1.2, -1.0)
	TestUniform(0.0, 100.0)

	TestUniformInt(1, 6)
	TestUniformInt(0, 4)
	TestUniformInt(-10, -2)
	TestUniformInt(-2, 4)
	TestUniformInt(-4, 0)
	TestUniformInt(3, 3)
	TestUniformInt(-7, -7)

	TestFlipCoin()

	TestBernoulli(0.5, 0.5)
	TestBernoulli(0.0, 0.0)
	TestBernoulli(0.2, 0.2)
	TestBernoulli(0.7, 0.7)
	TestBernoulli(1.0, 1.0)
	TestBernoulli(-1.1, 0.0)
	TestBernoulli(-100.0, 0.0)
	TestBernoulli(1.1, 1.0)
	TestBernoulli(100.0, 1.0)

	TestExponential(1.0)
	TestExponential(2.0)
	TestExponential(33.3)
	TestExponential(-1.0)
	TestExponential(-0.1)
	TestExponential(0.0)

	TestNormal(0.1, 1.0)
	TestNormal(1.0, 0.1)
	TestNormal(10.0, 3.0)
	TestNormal(-5.0, 3.0)

	TestShuffleEmpty()
	TestShuffle([999])
	TestShuffle([1, 2, 3, 4, 5, 6])
	TestShuffle(["hi", 42, true, 5.2, "bye", -1])
	TestShuffleLongAndDiverse(range(50))

	TestDrawOneEmpty()
	TestDrawOne([1, 2, 3, 4, 5])
	TestDrawOne(["sole"])
	TestDrawOne(["one", 2, PI, false])

	TestPointOnCircle(100.0, Vector2(0, 0))
	TestPointOnCircle(3481.3, Vector2(765, -999.2))
	TestPointOnCircle(0.2, Vector2(-6941.44, -0.4321))
	TestPointOnCircle(1.25, Vector2(10, 33))

	TestPointInDisk(100.0, Vector2(0, 0))
	TestPointInDisk(765.43, Vector2(-34, 88))
	TestPointInDisk(0.1, Vector2(-55.5, 55.5))
	TestPointInDisk(111.11, Vector2(111.1, 111.1))

	quit()


# Checks if two RNGs seeded with the same value generate the same sequence.
func TestSameSeed() -> void:
	var rng1 = RNG.new(1234)
	var rng2 = RNG.new(1234)

	for _i in range(N):
		Assert.isEqual(rng1.uniform(), rng2.uniform())


# Checks if two RNGs seeded with different values generate a different sequence.
func TestDifferentSeed() -> void:
	var rng1 = RNG.new(1234)
	var rng2 = RNG.new(4321)

	var allTheSame := true

	for _i in range(N):
		if rng1 != rng2:
			allTheSame = false
			break

	Assert.isFalse(allTheSame)


# Tests uniform() with the default parameters.
func TestUniformDefault() -> void:
	var rng = RNG.new()

	for _i in range(N):
		var u = rng.uniform()
		Assert.isGE(u, 0.0)
		Assert.isLT(u, 1.0)


# Tests uniform() with given parameters.
func TestUniform(a: float, b: float) -> void:
	var rng = RNG.new()

	for _i in range(N):
		var u = rng.uniform(a, b)
		Assert.isGE(u, a)
		Assert.isLT(u, b)


# Tests uniformInt() with given parameters. We just check if all numbers within
# the requested interval were generated at least once. No real check for
# uniformity, but better than nothing. (Will fail if (b - a) is not much smaller
# than N).
func TestUniformInt(a: int, b: int) -> void:
	var rng = RNG.new()

	var wasGenerated := { }

	for i in range(a, b+1):
		wasGenerated[i] = false

	for _i in range(N):
		var u = rng.uniformInt(a, b)
		Assert.isGE(u, a)
		Assert.isLE(u, b)
		wasGenerated[u] = true

	for i in range(a, b+1):
		Assert.isTrue(wasGenerated[i])


# Tests flipCoin().
func TestFlipCoin() -> void:
	var rng = RNG.new()

	var numTrues := 0

	for _i in range(N):
		if rng.flipCoin():
			numTrues += 1

	var actualP: float = float(numTrues) / N
	Assert.isSmall(abs(actualP - 0.5), 0.05)



# Tests bernoulli() with a given p. The other parameter, expectedP is the
# expected proportion of trues; it should normally be equal to p itself --
# except when p is not in [0.0, 1.0]
func TestBernoulli(p: float, expectedP: float) -> void:
	var rng = RNG.new()

	var numTrues := 0

	for _i in range(N):
		if rng.bernoulli(p):
			numTrues += 1

	var actualP: float = float(numTrues) / N
	Assert.isSmall(abs(actualP - expectedP), 0.05)


# Tests exponential() with a given mean -- er, kinda. I don't make any effort to
# ensure that the numbers are really drawn from an exponential distribution.
func TestExponential(mean: float) -> void:
	var rng = RNG.new()
	var vals = [ ]

	for _i in range(N):
		vals.append(rng.exponential(mean))

	var actualMean = Stats.mean(vals)

	var v : = abs(mean - actualMean)
	if mean > 0.0:
		v /= mean

	Assert.isSmall(v, 0.05)


# Tests normal() with given mean and standard deviation. Again, I don't make any
# effort to ensure that the numbers are really drawn from a normal distribution.
func TestNormal(mean: float, stdDev: float) -> void:
	var N := 250000 # override global N
	var rng = RNG.new()
	var vals = [ ]

	for _i in range(N):
		vals.append(rng.normal(mean, stdDev))

	var actualMean = Stats.mean(vals)
	Assert.isClose(mean, actualMean, 0.075)

	var actualStdDev = Stats.standardDeviation(vals, actualMean)
	Assert.isClose(stdDev, actualStdDev, 0.075)


# Tests calling shuffle on the array a.
func TestShuffle(a: Array) -> void:
	var rng = RNG.new()
	var aa = a.duplicate()
	var sa = rng.shuffle(a)
	Assert.isTrue(_checkShuffledElements(aa, sa))
	Assert.isTrue(sa == a)


# Tests calling shuffle on an empty array.
func TestShuffleEmpty() -> void:
	var rng = RNG.new()
	var a := [ ]
	var aa := a.duplicate()
	var sa = rng.shuffle(a)
	Assert.isEqual(a, [ ])
	Assert.isEqual(aa, sa)
	Assert.isTrue(sa == a)


# Tests calling shuffle on the array a, assumed to have a large number of
# elements that are diverse (that is, doesn't have many duplicates). In addition
# to what other tests already do, this checks if the shuffled array is different
# than the original array.
func TestShuffleLongAndDiverse(a: Array) -> void:
	var aa = a.duplicate()
	var rng = RNG.new()
	var sa = rng.shuffle(a)
	Assert.isTrue(_checkShuffledElements(a, sa))
	Assert.isNotEqual(aa, sa)
	Assert.isTrue(sa == a)


# Checks if the array a and the shuffled array sa have the same elements.
func _checkShuffledElements(a: Array, sa: Array) -> bool:
	var vac = { } # values and counts
	for e in a:
		if !vac.has(e):
			vac[e] = 0
		vac[e] += 1

	for e in sa:
		if !vac.has(e):
			return false
		vac[e] -= 1

	for count in vac.values():
		if count != 0:
			return false

	return true


# Checks if drawOne() works more or less as expected. We just draw many times,
# checking if the drawn element is always from the array argument. Additionally,
# we ensure that every value from the array is drawn at least one (though we
# don't make any effort to check if the distribution is uniform).
func TestDrawOne(a: Array) -> void:
	var rng = RNG.new()

	var wasDrawn := { }

	for e in a:
		wasDrawn[e] = false

	for _i in range(N):
		var e = rng.drawOne(a)
		Assert.isTrue(a.has(e))
		wasDrawn[e] = true

	for e in a:
		Assert.isTrue(wasDrawn[e])


# Checks that drawOne([]) == null.
func TestDrawOneEmpty() -> void:
	var rng = RNG.new()
	Assert.isNull(rng.drawOne([]))


# Tests pointOnCircle() with given parameters. Just sanity checks, no real
# randomness verifications.
func TestPointOnCircle(radius: float, center: Vector2) -> void:
	var rng = RNG.new()
	for _i in range(N):
		var p: Vector2 = rng.pointOnCircle(radius, center)
		Assert.isClose(radius, center.distance_to(p), 0.01)


# Tests pointInDisk() with given parameters. Just sanity checks, no real
# randomness verifications.
func TestPointInDisk(radius: float, center: Vector2) -> void:
	var rng = RNG.new()
	for _i in range(N):
		var p: Vector2 = rng.pointInDisk(radius, center)
		Assert.isLT(center.distance_to(p), radius)
